//
//  Logger.swift
//  HMLogger
//
//  Created by Harry Hsu on 2019/7/10.
//  Copyright © 2019 Dayu. All rights reserved.
//

import Foundation

public class Logger: NSObject {
  public func log(_ message: String) {
    print("message: \(message)")
  }
}
