//
//  HMLogger.h
//  HMLogger
//
//  Created by Harry Hsu on 2019/7/10.
//  Copyright © 2019 Dayu. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HMLogger.
FOUNDATION_EXPORT double HMLoggerVersionNumber;

//! Project version string for HMLogger.
FOUNDATION_EXPORT const unsigned char HMLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HMLogger/PublicHeader.h>


